#include <vector>
#include <iostream>
#include "state.h"
#include "except.h"

// ///////////////////////////////////////////////////////////////////////////

State::State(int order) : _order(order) {

    // What is the max index for linear representation of matrix
    // with given order
    _maxIndex = _order * _order;

    // Allocate new array for handling state
    _storage.reset(new Mark[_maxIndex]);

    // Set all elements to EMPTY state
    for (int i = 0; i < _maxIndex; i++) {
        _storage[i] = EMPTY;
    }
}

// ///////////////////////////////////////////////////////////////////////////

// Copy constructor
State::State(const State &state) {

    _order = state._order;
    _maxIndex = state._maxIndex;
    _storage.reset(new Mark[_maxIndex]);
    for (int i = 0; i < _maxIndex; i++) {
        _storage[i] = state._storage[i];
    }

}

// ///////////////////////////////////////////////////////////////////////////

// Move constructor
State::State(State &&other) {

    *this = std::move(other);

}

// ///////////////////////////////////////////////////////////////////////////

State::~State() {

    // Release current state
    _storage.reset(nullptr);
}

// ///////////////////////////////////////////////////////////////////////////

// Move assignment operator
State &State::operator=(State &&other) noexcept {

    if (this != &other) {
        _order = other._order;
        _maxIndex = other._maxIndex;
        _storage = std::move(other._storage);
    }

    return *this;
}

// ///////////////////////////////////////////////////////////////////////////

bool State::isValidPosition(int position) const {

    // The valid state is when position is from interval [0, _maxIndex)
    return position >= 0 && position < _maxIndex;
}

// ///////////////////////////////////////////////////////////////////////////

bool State::isValidMove(int position) const {

    // The valid move is when the position is valid and points for empty state
    return isValidPosition(position) ? _storage[position] == EMPTY : false;
}

// ///////////////////////////////////////////////////////////////////////////

Mark State::get(int position) const {

    // Do not process when wrong position is entered
    if (!isValidPosition(position)) {
        throw IllegalBoardPosition();
    }

    // Return the current state
    return _storage[position];
}

// ///////////////////////////////////////////////////////////////////////////

std::vector<int> State::indicesForRow(int row) const {

    std::vector<int> result;

    // Copy marks for selected row
    int i0 = row * _order;
    for (int i = 0; i < _order; i++) {
        result.push_back(i0 + i);
    }

    return result;
}

// ///////////////////////////////////////////////////////////////////////////

void State::set(Mark mark, int position) {

    // Do not process when wrong position is entered
    if (!isValidPosition(position)) {
        throw IllegalBoardPosition();
    }

    // Change state on given position
    _storage[position] = mark;
}

// ///////////////////////////////////////////////////////////////////////////

std::vector<int> State::allEmptyIndices() const {

    std::vector<int> result;

    // Find all positions with empty mark
    for (int i = 0; i < _maxIndex; i++) {
        if (_storage[i] == EMPTY) {
            result.push_back(i);
        }
    }

    return result;
}

// ///////////////////////////////////////////////////////////////////////////

bool State::hasFullDiagonal(Mark mark) const {

    // Check main-diagonal elements if given sign presents
    // Matrix elements (0,0), (1,1), (2,2) => (0),(4),(8)
    // So, every order+1 element from linear storage should be checked
    bool result = true;
    for (int i = 0; i < _maxIndex; i += _order + 1) {
        //std::cout << i << " = " << _storage[i] << std::endl;
        result = result && (_storage[i] == mark);
        // Do not continue when the result has changed
        if (!result) {
            break;
        }
    }

    if (!result) {
        // Check anti-diagonal elements if given sign presents
        // Matrix elements (2,0), (1,1), (0,2) => (6),(4),(2)
        // So, every order-1 element from linear storage should be checked
        result = true;
        for (int i = _order - 1; i < _maxIndex - _order + 1; i += _order - 1) {
            //std::cout << i << " = " << _storage[i] << std::endl;
            result = result && (_storage[i] == mark);
            // Do not continue when the result has changed
            if (!result) {
                return false;
            }
        }
    }

    // Default
    return true;
}

// ///////////////////////////////////////////////////////////////////////////

bool State::hasFullRow(Mark mark) const {

    // Check row by row if given sign is present in each column
    // Matrix elements (0,0), (0,1), (0,2) => 0 + (0),(1),(2)
    // Matrix elements (1,0), (1,1), (1,2) => 3 + (0),(1),(2)
    // Matrix elements (2,0), (2,1), (2,2) => 6 + (0),(1),(2)
    for (int row = 0; row < _order; row++) {
        bool result = true;
        int rowStartIndex = row * _order;
        for (int col = 0; col < _order; col++) {
            //std::cout << rowStartIndex + col << " = " << _storage[rowStartIndex + col] << std::endl;
            result = result && (_storage[rowStartIndex + col] == mark);
            // Do not continue when the result has changed
            if (!result) {
                break;
            }
        }
        // Stop - if this row is full of same sign
        if (result) {
            return true;
        }
    }

    // Default
    return false;
}

// ///////////////////////////////////////////////////////////////////////////

bool State::hasFullColumn(Mark mark) const {

    // Check row by row if given sign is present in each column
    // Matrix elements (0,0), (1,0), (2,0) => (0),(3),(6)
    // Matrix elements (0,1), (1,1), (2,1) => (1),(4),(7)
    // Matrix elements (0,2), (1,2), (2,2) => (2),(5),(8)
    // So, for each column, row indices are calculated as
    // => col + 0*order, col + 1*order, col+ 2*order, ...
    for (int col = 0; col < _order; col++) {
        bool result = true;
        for (int i = 0; i < _order; i++) {
            //std::cout << col + i * _order << " = " << _storage[col + i * _order] << std::endl;
            result = result && (_storage[col + i * _order] == mark);
            // Do not continue when the result has changed
            if (!result) {
                break;
            }
        }
        // Stop - if this column is full of same sign
        if (result) {
            return true;
        }
    }

    // Default
    return false;
}

// ///////////////////////////////////////////////////////////////////////////
