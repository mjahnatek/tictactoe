#ifndef TICTACTOE_STATE_H
#define TICTACTOE_STATE_H

#include <array>
#include "mark.h"

class State {

private:

    int _order;

    int _maxIndex;

    std::unique_ptr<Mark[]> _storage;

public:

    explicit State(int order);

    State(const State &state);

    State(State &&other);

    virtual ~State();

    int order() const { return _order; }

    bool isValidPosition(int) const;

    bool isValidMove(int) const;

    Mark get(int position) const;

    std::vector<int> indicesForRow(int row) const;

    void set(Mark mark, int position);

    std::vector<int> allEmptyIndices() const;

    bool hasFullDiagonal(Mark mark) const;

    bool hasFullRow(Mark mark) const;

    bool hasFullColumn(Mark mark) const;

    State &operator=(State &&other) noexcept;

};

#endif //TICTACTOE_STATE_H
