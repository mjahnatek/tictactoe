#ifndef TICTACTOE_MOVE_H
#define TICTACTOE_MOVE_H

#include <sys/param.h>
#include "state.h"

class Move {

private:

    Mark _mark;

    int _position;

    int _score;

public:

    Move(Mark mark, int position);

    Move(int position, int score);

    Move(Mark mark, int position, int score);

    virtual ~Move();

    void setMark(Mark mark) { _mark = mark; }

    void setPosition(int position) { _position = position; }

    Mark getMark() const { return _mark; }

    int getPosition() const { return _position; }

    int getScore() const { return _score; }

    void setScore(int score) { _score = score; }

    void setIfMax(int position, int score);

    void setIfMin(int position, int score);

};

#endif //TICTACTOE_MOVE_H
