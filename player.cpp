#include <iostream>
#include <vector>
#include <err.h>
#include <random>
#include "player.h"
#include "except.h"

// ///////////////////////////////////////////////////////////////////////////

Player::Player(const Mark mark) : _mark(mark) {}

// ///////////////////////////////////////////////////////////////////////////
// Human player
// ///////////////////////////////////////////////////////////////////////////

HumanPlayer::HumanPlayer(const Mark mark) : Player(mark) {}

Move HumanPlayer::nextMove(const Board &board) {

    // Do not continue when board is full
    if (!board.areAnyMovesAvailable()) {
        throw NoneMovePossible();
    }

    // Ask user for new position - until the valid one is entered
    std::string answer;
    int position = -1;
    do {

        // Get user's input
        std::cout << "Select (q) for quit, (s) skip move, or set position: " << std::endl;
        std::cin >> answer;

        // Transform to upper case for comparison
        std::transform(answer.begin(), answer.end(), answer.begin(), ::toupper);

        // Throw special exception for skipped move state
        if (answer.compare("S") == 0) {
            throw HumanSkippedMove();
        }

        // Throw special exception for quit
        if (answer.compare("Q") == 0) {
            throw HumanQuitGame();
        }

        // Try to convert into number and use it as position on board
        try {
            position = std::stoi(answer);
        } catch (std::invalid_argument &e) {
            std::cerr << "Wrong input, try again..." << std::endl;
        }

    } while (!board.isValidMove(position - 1));

    // Return new move
    return Move(this->_mark, position - 1);
}

// ///////////////////////////////////////////////////////////////////////////
// Computer player (random move variant)
// ///////////////////////////////////////////////////////////////////////////

ComputerRandomPlayer::ComputerRandomPlayer(const Mark mark) : Player(mark) {}

Move ComputerRandomPlayer::nextMove(const Board &board) {

    // Do not continue when board is full
    if (!board.areAnyMovesAvailable()) {
        throw NoneMovePossible();
    }

    // Get all empty positions
    std::vector<int> possiblePositions = board.emptyPositions();

    // Random
    std::random_device rd;
    std::mt19937 rng(rd());
    std::uniform_int_distribution<unsigned long> uni(0,possiblePositions.size());

    // Select randomly one of them
    int position;
    do {
        position = static_cast<int>(uni(rng));
    } while (!board.isValidMove(possiblePositions[position]));

    // Return new move
    return Move(this->_mark, possiblePositions[position]);
}

// ///////////////////////////////////////////////////////////////////////////
// Computer player - basic minmax variant
// ///////////////////////////////////////////////////////////////////////////

ComputerMinMaxPlayer::ComputerMinMaxPlayer(Mark mark) : Player(mark) {}

Move ComputerMinMaxPlayer::nextMove(const Board &board) {

    AISolver solver(board, _mark);
    return solver.findBestMove(MINIMAX);

}

// ///////////////////////////////////////////////////////////////////////////
// Computer player - alpha-beta pruning variant
// ///////////////////////////////////////////////////////////////////////////

ComputerAlphaBetaPruningPlayer::ComputerAlphaBetaPruningPlayer(Mark mark) : Player(mark) {}

Move ComputerAlphaBetaPruningPlayer::nextMove(const Board &board) {

    AISolver solver(board, _mark);
    return solver.findBestMove(ALPHA_BETA_PRUNINIG);

}

// ///////////////////////////////////////////////////////////////////////////
