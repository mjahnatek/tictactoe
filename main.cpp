#include <iostream>
#include <list>
#include "player.h"
#include "except.h"

int main() {

    // TIC-TAC-TOE is matrix of order 3
    Board board(3);

    // Create and setup players
    std::shared_ptr<Player> player1(new HumanPlayer(CROSS));
    //std::shared_ptr<Player> player2(new ComputerRandomPlayer(NOUGHT));
    //std::shared_ptr<Player> player2(new ComputerMinMaxPlayer(NOUGHT));
    std::shared_ptr<Player> player2(new ComputerAlphaBetaPruningPlayer(NOUGHT));

    // Game loop
    std::list<std::shared_ptr<Player>> players{player1, player2};
    auto currentPlayer = players.begin();
    while (board.areAnyMovesAvailable()) {

        try {

            // Show current state board
            std::cout << board << std::endl;

            // Obtain next move from current player
            Move move = (*currentPlayer)->nextMove(board);

            // Perform move
            board << move;

            // Show info about move
            std::cout << "Player " << MarkHelper::stringify(move.getMark())
                      << " moves to " << (move.getPosition() + 1) << std::endl << std::endl;

            // Check game state for current player
            if (board.isWinner(move.getMark())) {
                std::cout << board << std::endl;
                std::cout << "Player " << MarkHelper::stringify(move.getMark()) << " has WIN!" << std::endl;
                break;
            }

        } catch (HumanQuitGame &e) {

            std::cout << "Quit! Bye!" << std::endl;
            break;

        } catch (HumanSkippedMove &e) {

            // Change players' board signs and let the second player to play
            player1->setMark(MarkHelper::opposite(player1->getMark()));
            player2->setMark(MarkHelper::opposite(player2->getMark()));
            std::cout << "Players' sign has been exchanged." << std::endl;

        } catch (NoneMovePossible &e) {

            // This should never happen
            std::cerr << "Illegal state - asked for move with closed board state" << std::endl;
            break;

        }

        // Next player (cyclic list)
        currentPlayer++;
        if (currentPlayer == players.end()) {
            currentPlayer = players.begin();
        }

    }

    // If the game loop has ended and there are not any moves available -> it must be tie!
    if (!board.areAnyMovesAvailable()) {
        std::cout << board << std::endl;
        std::cout << "TIE!" << std::endl;
    }

    // Exit
    return 0;
}