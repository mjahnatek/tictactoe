#ifndef TICTACTOE_MARK_H
#define TICTACTOE_MARK_H

#include <string>

enum Mark {
    EMPTY, NOUGHT, CROSS
};

struct MarkHelper {

    static std::string stringify(Mark mark) {

        switch (mark) {

            case CROSS:
                return "X";
            case NOUGHT:
                return "O";
            case EMPTY:
                return "-";
        }

    }

    static Mark opposite(Mark mark) {
        return mark == EMPTY ? EMPTY : (mark == CROSS ? NOUGHT : CROSS);
    }

};

#endif //TICTACTOE_MARK_H
