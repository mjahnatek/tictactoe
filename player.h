#ifndef TICTACTOE_PLAYER_H
#define TICTACTOE_PLAYER_H

#include "state.h"
#include "move.h"
#include "board.h"
#include "aisolver.h"

// ///////////////////////////////////////////////////////////////////////////

class Player {

protected:

    Mark _mark;

public:

    explicit Player(Mark mark);

    virtual ~Player() = default;

    void setMark(Mark _mark) { this->_mark = _mark; }

    Mark getMark() const { return _mark; }

    virtual Move nextMove(const Board &board)= 0;

};

// ///////////////////////////////////////////////////////////////////////////

class HumanPlayer : public Player {

public:

    explicit HumanPlayer(Mark mark);

    ~HumanPlayer() override = default;

    Move nextMove(const Board &board) override;

};

// ///////////////////////////////////////////////////////////////////////////

class ComputerRandomPlayer : public Player {

public:

    explicit ComputerRandomPlayer(Mark mark);

    ~ComputerRandomPlayer() override = default;

    Move nextMove(const Board &board) override;

};

// ///////////////////////////////////////////////////////////////////////////

class ComputerMinMaxPlayer : public Player {

public:

    explicit ComputerMinMaxPlayer(Mark mark);

    ~ComputerMinMaxPlayer() override = default;

    Move nextMove(const Board &board) override;

};

// ///////////////////////////////////////////////////////////////////////////

class ComputerAlphaBetaPruningPlayer : public Player {

public:

    explicit ComputerAlphaBetaPruningPlayer(Mark mark);

    ~ComputerAlphaBetaPruningPlayer() override = default;

    Move nextMove(const Board &board) override;

};

// ///////////////////////////////////////////////////////////////////////////

#endif //TICTACTOE_PLAYER_H
