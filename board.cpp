#include <iostream>
#include "board.h"
#include "except.h"

// ///////////////////////////////////////////////////////////////////////////

Board::Board(int order) : _state(order) {}

Board::Board(const Board &board) : _state(board._state) {}

// ///////////////////////////////////////////////////////////////////////////

std::string Board::markAtPosition(int position) const {

    try {
        switch (_state.get(position)) {

            case CROSS:
                return MarkHelper::stringify(CROSS);
            case NOUGHT:
                return MarkHelper::stringify(NOUGHT);
            case EMPTY:
                return std::to_string(position + 1);
        }
    } catch (IllegalBoardPosition &e) {
        return "?";
    }

}

// ///////////////////////////////////////////////////////////////////////////

std::string centeredString(std::string const &str, unsigned long width) {
    unsigned long padding = (width - str.size()) / 2;
    return padding > 0 ? std::string(padding + ((width - 2 * padding) - str.size()), ' ')
                          + str + std::string(padding, ' ') : str;
}

// ///////////////////////////////////////////////////////////////////////////

std::ostream &operator<<(std::ostream &stream, const Board &board) {

    // Padding
    unsigned long padding = std::to_string(board.order() * board.order()).size() + 4;

    // Header
    for (int i = 0; i < board.order(); i++) {
        if (i != 0) {
            stream << ".";
        }
        stream << std::string(padding, ' ');
    }
    stream << std::endl;

    // Content
    for (int row = 0; row < board.order(); row++) {

        if (row != 0) {
            // -----+-----+-----
            for (int i = 0; i < board.order(); i++) {
                if (i != 0) {
                    stream << "+";
                }
                stream << std::string(padding, '-');
            }
            stream << std::endl;
        }

        // 1  |  2  |  3
        std::vector<int> ri = board.rowPositions(row);
        for (int i = 0; i < ri.size(); i++) {
            if (i != 0) {
                stream << "|";
            }
            stream << centeredString(board.markAtPosition(ri[i]), padding);
        }
        stream << std::endl;

    }

    // Footer
    for (int i = 0; i < board.order(); i++) {
        if (i != 0) {
            stream << "'";
        }
        stream << std::string(padding, ' ');
    }
    stream << std::endl;

    //
    return stream;
}

// ///////////////////////////////////////////////////////////////////////////

Board &operator<<(Board &board, const Move &move) {

    try {
        board.applyMove(move);
    } catch (IllegalBoardPosition &e) {
        std::cerr << "Wrong move position" << std::endl;
    }

    return board;
}

// ///////////////////////////////////////////////////////////////////////////

bool Board::isWinner(Mark mark) const {

    if (_state.hasFullDiagonal(mark)) {
        return true;
    } else if (_state.hasFullColumn(mark)) {
        return true;
    } else if (_state.hasFullRow(mark)) {
        return true;
    } else {
        return false;
    }
}

// ///////////////////////////////////////////////////////////////////////////
