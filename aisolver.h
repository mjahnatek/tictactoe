#ifndef TICTACTOE_AISOLVER_H
#define TICTACTOE_AISOLVER_H

#include "state.h"
#include "move.h"
#include "board.h"

enum AISolverAlgorithm {
    MINIMAX, ALPHA_BETA_PRUNINIG
};

class AISolver {

    Mark _mark;

    Board _board;

    Move _bestMove;

private:

    int minimax(int deep, bool isMax);

    int alphaBetaPruning(int deep, bool isMax, int alpha, int beta);

public:

    explicit AISolver(const Board &board, Mark mark);

    Move findBestMove(AISolverAlgorithm algorithm);

};


#endif //TICTACTOE_AISOLVER_H
