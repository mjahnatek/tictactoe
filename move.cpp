#include "move.h"

// ///////////////////////////////////////////////////////////////////////////

Move::Move(Mark mark, int position) : _mark(mark), _position(position) {}

Move::Move(int position, int score) : _mark(EMPTY), _position(position), _score(score) {}

Move::Move(Mark mark, int position, int score) : _mark(mark), _position(position), _score(score) {}

// ///////////////////////////////////////////////////////////////////////////

Move::~Move() {}

// ///////////////////////////////////////////////////////////////////////////

void Move::setIfMax(int position, int score) {

    if (score > _score) {
        _score = score;
        _position = position;
    }

}

// ///////////////////////////////////////////////////////////////////////////

void Move::setIfMin(int position, int score) {

    if (score < _score) {
        _score = score;
        _position = position;
    }

}

// ///////////////////////////////////////////////////////////////////////////
