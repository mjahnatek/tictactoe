#include <vector>
#include "aisolver.h"

#define MAX_VALUE 1000

// ///////////////////////////////////////////////////////////////////////////

AISolver::AISolver(const Board &board, Mark mark) : _board(board), _mark(mark), _bestMove(mark, -1, 0) {}

// ///////////////////////////////////////////////////////////////////////////

Move AISolver::findBestMove(AISolverAlgorithm algorithm) {

    _bestMove.setScore(-MAX_VALUE);

    std::vector<int> possiblePositions = _board.emptyPositions();
    for (int position : possiblePositions) {

        // Apply move
        _board.setMarkAt(_mark, position);

        // Save move if its score is better
        if (algorithm == MINIMAX) {
            _bestMove.setIfMax(position, minimax(0, false));
        } else {
            _bestMove.setIfMax(position, alphaBetaPruning(0, false, -MAX_VALUE, MAX_VALUE));
        }

        // Take move back
        _board.setMarkAt(EMPTY, position);
    }

    return _bestMove;
}

// ///////////////////////////////////////////////////////////////////////////

int AISolver::minimax(int deep, bool isMax) {

    // Check game over states
    if (_board.isWinner(_mark)) {
        return MAX_VALUE - deep;
    } else if (_board.isWinner(MarkHelper::opposite(_mark))) {
        return -MAX_VALUE + deep;
    }

    // Get all possible moves for this board state
    std::vector<int> possiblePositions = _board.emptyPositions();

    // If there is no winner and none next move - it must be tie!
    if (possiblePositions.empty()) {
        return 0;
    }

    // If order is > 3 (the number of combinations is too much)
    // limit deep to obtain some result in reasonable time
    if( _board.order() > 3 && deep > 2) {
        return 0;
    }

    // Investigate all possible moves
    int best = isMax ? -MAX_VALUE : MAX_VALUE;
    int score;
    for (int position : possiblePositions) {

        // Apply move
        _board.setMarkAt(isMax ? _mark : MarkHelper::opposite(_mark), position);

        // Use minmax for current board state evaluation
        score = minimax(deep + 1, !isMax);
        best = isMax ? MAX(best, score) : MIN(best, score);

        // Undo move
        _board.setMarkAt(EMPTY, position);
    }

    // Return best score
    return best;
}

// ///////////////////////////////////////////////////////////////////////////

int AISolver::alphaBetaPruning(int deep, bool isMax, int alpha, int beta) {

    // Check game over states
    if (_board.isWinner(_mark)) {
        return MAX_VALUE - deep;
    } else if (_board.isWinner(MarkHelper::opposite(_mark))) {
        return -MAX_VALUE + deep;
    }

    // Get all possible moves for this board state
    std::vector<int> possiblePositions = _board.emptyPositions();

    // If there is no winner and none next move - it must be tie!
    if (possiblePositions.empty()) {
        return 0;
    }

    // If order is > 3 (the number of combinations is too much)
    // limit deep to obtain some result in reasonable time
    if( _board.order() > 3 && deep > 4) {
        return 0;
    }

    // Investigate all possible moves
    int score;
    for (int position : possiblePositions) {

        // Apply move
        _board.setMarkAt(isMax ? _mark : MarkHelper::opposite(_mark), position);

        // Find max (and store it in alpha), find min (and store it in beta)
        score = alphaBetaPruning(deep + 1, !isMax, alpha, beta);
        if (isMax) {
            alpha = MAX(alpha, score);
        } else {
            beta = MIN(beta, score);
        }

        // Undo move
        _board.setMarkAt(EMPTY, position);

        // Alpha-beta cutoff (pruning)
        if (alpha >= beta) {
            break;
        }
    }

    // Return best score
    return isMax ? alpha : beta;
}

// ///////////////////////////////////////////////////////////////////////////
