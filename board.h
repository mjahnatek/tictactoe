#ifndef TICTACTOE_BOARD_H
#define TICTACTOE_BOARD_H

#include <iostream>
#include <vector>
#include <list>
#include "state.h"
#include "move.h"

class Board {

private:

    State _state;

public:

    explicit Board(int order);

    Board(const Board &board);

    int order() const { return _state.order(); }

    std::string markAtPosition(int position) const;

    bool isValidMove(int position) const { return _state.isValidMove(position); }

    bool areAnyMovesAvailable() const { return !_state.allEmptyIndices().empty(); }

    void applyMove(const Move &move) { _state.set(move.getMark(), move.getPosition()); }

    void setMarkAt(Mark mark, int position) { _state.set(mark, position); }

    std::vector<int> emptyPositions() const { return _state.allEmptyIndices(); }

    bool isWinner(Mark mark) const;

    std::vector<int> rowPositions(int row) const { return _state.indicesForRow(row); }

    friend Board &operator<<(Board &board, const Move &move);

    friend std::ostream &operator<<(std::ostream &stream, const Board &board);

};


#endif //TICTACTOE_BOARD_H
